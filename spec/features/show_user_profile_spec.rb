require 'rails_helper'

RSpec.feature 'Show a user profile', type: :feature do

  background do
    user = create(:user, user_name: 'First User')
    second_user = create(:user, id: user.id + 1, user_name: 'Second User', email: 'second@user.com')

    first_post = create(:post, user_id: user.id)
    second_post = create(:post, user_id: second_user.id, caption: 'This is my post')

    sign_in_with user

    visit '/'
    first('.user-name').click_link('First User')
  end

  scenario 'the profile page contains the user name on the url' do
    expect(page.current_path).to eq(profile_path('First User'))
  end

  scenario 'the profile page contains all the user\'s posts' do
    expect(page).to have_content 'MyString'
    expect(page).not_to have_content 'This is my post'
  end

end
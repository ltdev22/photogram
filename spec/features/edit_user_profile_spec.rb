require 'rails_helper'

RSpec.feature 'Edit a user profile', type: :feature do

  background do
    user = create(:user, user_name: 'FirstUser')
    second_user = create(:user, id: user.id + 1, user_name: 'SecondUser', email: 'second@user.com')

    first_post = create(:post, user_id: user.id)
    second_post = create(:post, user_id: second_user.id, caption: 'This is my post')

    sign_in_with user

    visit '/'
  end

  scenario 'a user can edit his profile details' do
    first(:link, 'FirstUser').click
    click_link('Edit Profile')

    attach_file('user_avatar', 'spec/files/img/profile-avatar.png')
    fill_in 'user_bio', with: 'This is my profile'
    click_button 'Edit Profile'

    expect(page.current_path).to eq profile_path('FirstUser')
    expect(page).to have_content('Profile was successfully updated')
    expect(page).to have_css("img[src*='profile-avatar']")
    expect(page).to have_content('This is my profile')
  end

  scenario 'a user can\'t edit another\'s user profile' do
    first(:link, 'SecondUser').click
    expect(page).not_to have_content('Edit Profile')
  end

  scenario 'a user can\'t access directly and edit the profile' do
    visit 'SecondUser/edit'

    expect(page).not_to have_content('Upload your avatar')
    expect(page.current_path).to eq(profile_path('SecondUser'))
    expect(page).to have_content('You are not authorized to edit this profile')
  end

end
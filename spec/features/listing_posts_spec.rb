require 'rails_helper'

RSpec.feature 'Listing posts', :type => :feature do
  
  scenario 'Index displays a list of posts' do
    user = create(:user)
    post1 = create(:post, caption: 'Hello World! This is the first post', user_id: user.id)
    post2 = create(:post, caption: 'This is the second post', user_id: user.id)

    sign_in_with(user)

    expect(page).to have_css("img[src*='hello-world.jpg']")
    expect(page).to have_content('Hello World! This is the first post')
    expect(page).to have_content('This is the second post')
  end

end
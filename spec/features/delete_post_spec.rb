require 'rails_helper'

RSpec.feature 'Deleting a post', :type => :feature do

  background do
    user = create(:user)
    post = create(:post, caption: 'This is my first post', user_id: user.id)
    
    sign_in_with(user)

    find(:xpath, "//a[@href='/posts/1']").click
    click_link('Delete')
  end
  
  scenario 'User can delete a post' do
    expect(page).to have_content('Post was successfully destroyed')
    expect(page).not_to have_content('This is my first post')
    expect(page.current_path).to eq(root_path)
  end

end
require 'rails_helper'

RSpec.feature 'Editing a post', :type => :feature do

  background do
    @user = create(:user)
    @user_2 = create(:user, id: @user.id + 1, user_name: 'Demo Two', email: 'two@demo.com')
    
    @post = create(:post, user_id: @user.id)
    @post_2 = create(:post, user_id: @user.id + 1)

    sign_in_with(@user)

    visit '/'
  end
  
  scenario 'User can edit his post' do
    find(:xpath, "//a[@href='/posts/1']").click
    click_link('Edit', href: edit_post_path(@post))

    fill_in 'Caption', with: 'This is an updated post'
    click_button 'Update Post'

    expect(page).to have_content('Post was successfully updated')
    expect(page).to have_content('This is an updated post')
  end

  scenario 'User can\'t edit a post of other users' do
    find(:xpath, "//a[@href='/posts/2']").click

    expect(page).to_not have_content('Edit')
  end

  scenario 'User can\'t edit a post of other users via url path' do
    visit edit_post_path(@post_2)

    expect(page).to have_content('You are not authorized to edit this post')
    expect(page.current_path).to eq root_path
  end

  scenario 'User cannot edit a post without caption' do
    find(:xpath, "//a[@href='/posts/1']").click
    click_link('Edit', href: edit_post_path(@post))

    fill_in 'Caption', with: ''
    click_button 'Update Post'

    expect(page).to have_content('Please review the problems below')
    expect(page).to have_content('Caption can\'t be blank')
  end

end
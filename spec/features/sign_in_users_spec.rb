require 'rails_helper'

RSpec.feature 'Sign in users', type: :feature do
  
  background do
    @user = create(:user)
  end

  scenario 'a user can login successfully' do
    visit '/'
    expect(page).not_to have_content('New Post')
    click_link 'Sign In'

    fill_in 'Email', with: @user.email
    fill_in 'Password', with: @user.password
    click_button 'Log in'

    expect(page).not_to have_content('Sign In')
    expect(page).not_to have_content('Sign Up')
    expect(page).to have_content('Signed in successfully')
  end

  scenario 'a user can log out successfully' do
    visit '/'
    click_link 'Sign In'

    fill_in 'Email', with: @user.email
    fill_in 'Password', with: @user.password
    click_button 'Log in'

    click_link 'Sign Out'
    expect(page).to have_content('Sign In')
    expect(page).to have_content('Sign Up')
    expect(page).to have_content('You need to sign in or sign up before continuing')
  end

  scenario 'a user must login to view posts' do
    visit '/'
    expect(page).to have_content('You need to sign in or sign up before continuing')
  end

  scenario 'a user can\'t create a post without logging in' do
    visit new_post_path
    expect(page).to have_content('You need to sign in or sign up before continuing')
  end
end
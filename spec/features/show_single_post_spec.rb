require 'rails_helper'

RSpec.feature 'Dsiplay individual posts', :type => :feature do
  
  scenario 'User can view a single post' do
    user = create(:user)
    post = create(:post, user_id: user.id)

    sign_in_with(user)
    
    find(:xpath, "//a[@href='/posts/1']").click

    expect(page).to have_css("img[src*='hello-world.jpg']")
    expect(page).to have_content(post.caption)
    expect(page.current_path).to eq(post_path(post))
  end

end
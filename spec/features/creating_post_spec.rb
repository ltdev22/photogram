require 'rails_helper'

RSpec.feature 'Creating Post', :type => :feature do

  background do
    user = create(:user)

    sign_in_with(user)

    click_link 'New Post'
  end

  scenario 'A user creates a new post' do
    attach_file('Image', 'spec/files/img/hello-world.jpg')
    fill_in 'Caption', with: 'Hello World! This is the first post!'
    click_button 'Create Post'

    expect(page).to have_content('Post was successfully created')
    expect(page).to have_css("img[src*='hello-world.jpg']")
    expect(page).to have_content('Hello World! This is the first post!')
    expect(page).to have_content('Demo User')
  end

  scenario 'A user cannot create new post without image' do
    click_button 'Create Post'
    
    expect(page).to have_content('Please review the problems below')
    expect(page).to have_content('Image can\'t be blank')
    expect(page).to have_content('Caption can\'t be blank')
  end

end
require 'rails_helper'

RSpec.feature 'Liking posts', type: :feature do

  background do
    user = create(:user)
    post = create(:post, user_id: user.id)

    sign_in_with user
    visit '/'
  end

  scenario 'a user can like a post' do
    click_link 'like_1'

    expect(find('.likes')).to have_content('Demo User')
  end

  scenario 'a user can unlike a post' do
    # Repeat same steps
    # Click the "like" button once more

    # Expect to see the "unlikes-post" class on the button
    # Expect to NOT see my name within the "liked by" area of the post
  end

end
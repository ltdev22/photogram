require 'rails_helper'

RSpec.feature 'Creating new comments', type: :feature do

  scenario 'a user can add a new comment to a post' do
    user = create(:user)
    post = create(:post, user_id: user.id)

    sign_in_with user

    visit '/'
    fill_in "comment_content_#{post.id}", with: 'This is a comment'
    click_button 'New Comment'

    expect(page).to have_content('Comment was successfully created')
    expect(page).to have_content('This is a comment')
  end

end
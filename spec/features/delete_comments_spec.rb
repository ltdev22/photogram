require 'rails_helper'

RSpec.feature 'Deleting comments', type: :feature do

  background do
    user_1 = create(:user)
    user_2 = create(:user, id: user_1.id + 1, user_name: 'Second User', email: 'second@user.com')

    post = create(:post)
    comment_1 = create(:comment, user_id: user_2.id, post_id: post.id, body: 'Delete this comment')
    comment_2 = create(:comment, id: comment_1.id + 1, post_id: post.id)

    sign_in_with user_2
  end

  scenario 'a user can delete his comments only' do
    visit '/'
    expect(page).to have_content('Delete this comment')

    click_link 'delete-1'

    expect(page).to have_content('Comment was successfully deleted')
    expect(page).not_to have_content('Delete this comment')
  end

  scenario 'a user can\'t delete a comment that doesn\'t belong to him' do
    visit '/'
    expect(page).to have_content('Hello World too! This is a comment')
    expect(page).not_to have_css('#delete-2')
  end

  scenario 'a user can\'t delete a comment that doesn\'t belong to him via url' do
    visit '/'
    expect(page).to have_content('Hello World too! This is a comment')
    
    page.driver.submit :delete, 'posts/1/comments/2', {}
    
    expect(page).to have_content('You are not authorized to delete this comment')
    expect(page).to have_content('Hello World too! This is a comment')
  end

end
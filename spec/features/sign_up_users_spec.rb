require 'rails_helper'

RSpec.feature 'Signup users', type: :feature do

  background do
    visit '/'
    click_link 'Sign Up'
  end

  scenario 'a user can register with valid credentials' do
    fill_in 'User name', with: 'Demo User'
    fill_in 'Email', with: 'demo@user.com'
    fill_in 'Password', with: 'secretpassword', match: :first
    fill_in 'Password confirmation', with: 'secretpassword'
    click_button 'Sign up'

    expect(page).to have_content('You have signed up successfully')
  end

  scenario 'a user can\'t register with invalid credentials' do
    fill_in 'User name', with: ''
    fill_in 'Email', with: 'demouser.com'
    fill_in 'Password', with: 'secretpassword', match: :first
    fill_in 'Password confirmation', with: 'secret'
    click_button 'Sign up'

    expect(page).to have_content('Please review the problems below')
    expect(page).to have_content('User name can\'t be blank')
    expect(page).to have_content('Email is invalid')
    expect(page).to have_content('Password confirmation doesn\'t match Password')
  end

  scenario 'a user can\'t register with too sort username' do
    fill_in 'User name', with: 'a'
    click_button 'Sign up'

    expect(page).to have_content('Please review the problems below')
    expect(page).to have_content('User name is too short')
  end

end
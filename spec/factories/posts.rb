FactoryGirl.define do
  factory :post do
    user_id 1
    caption "MyString"
    image Rack::Test::UploadedFile.new(Rails.root + 'spec/files/img/hello-world.jpg', 'image/jpg')
  end
end

FactoryGirl.define do
  factory :user do
    id 1
    user_name 'Demo User'
    email 'demouser@gmail.com'
    password 'secretpassword'
  end
end

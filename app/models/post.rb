# == Schema Information
#
# Table name: posts
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  caption    :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  image      :string(255)
#
# Indexes
#
#  index_posts_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_5b5ddfd518  (user_id => users.id)
#

class Post < ApplicationRecord

  default_scope { order('posts.created_at DESC') }

  acts_as_votable

  mount_uploader :image, PostImageUploader

  # Assocciations
  belongs_to :user
  has_many :comments, dependent: :destroy

  # Validations
  validates :user_id, presence: true
  validates :image, presence: true
  validates :caption, presence: true

end

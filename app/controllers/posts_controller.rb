class PostsController < ApplicationController

  before_action :authenticate_user!
  before_action :set_post, only: [:show, :edit, :update, :destroy, :like]
  before_action :authorize_posts, only: [:edit, :update, :destroy]

  def index
    @posts = Post.includes(:user, :comments).all.page(params[:page])
  end

  def show
  end

  def new
    @post = current_user.posts.build
  end

  def create
    @post = current_user.posts.build(post_params)

    respond_to do |format|
      if @post.save
        format.html { redirect_to @post, flash: { success: 'Post was successfully created.' } }
      else
        format.html { render :new }
      end
    end
  end

  def edit
  end

  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to post_path(@post), flash: { success: 'Post was successfully updated.' } }
      else
        format.html { render :edit }
      end
    end
  end

  def destroy
    @post.destroy

    respond_to do |format|
      format.html { redirect_to root_path, notice: 'Post was successfully destroyed.'}
    end
  end

  def like
    if @post.liked_by current_user
      respond_to do |format|
        format.html { redirect_back(fallback_location: root_path) }
        format.js
      end
    end
  end

  private

    def set_post
      @post = Post.find(params[:id])
    end

    def authorize_posts
      unless current_user == @post.user
        redirect_to root_path, flash: { alert: 'You are not authorized to edit this post.'}
      end
    end

    def post_params
      params.require(:post).permit(:caption, :image)
    end

end

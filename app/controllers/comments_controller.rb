class CommentsController < ApplicationController

  before_action :set_post

  def index
    @comments = @post.comments

    respond_to do |format|
      format.html { render layout: !request.xhr? }
    end
  end

  def create
    @comment = @post.comments.build(comment_parans)
    @comment.user = current_user

    if @comment.save
      flash[:success] = 'Comment was successfully created.'
    else
      flash[:error] = 'Comment was not created.'
    end
    redirect_back(fallback_location: root_path)
  end

  def destroy
    @comment = @post.comments.find(params[:id])

    if @comment.user == current_user
      @comment.destroy
      flash[:success] = 'Comment was successfully deleted.'
    else
      flash[:alert] = 'You are not authorized to delete this comment.'
    end
    redirect_to root_path
  end

  private

    def comment_parans
      params.require(:comment).permit(:body)
    end

    def set_post
      @post = Post.find(params[:post_id])
    end

end

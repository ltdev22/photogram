class ProfilesController < ApplicationController

  before_action :authenticate_user!
  before_action :set_user
  before_action :authorized_profile, only: [:edit, :update]

  def show
    @posts = @user.posts.includes(:comments)
  end

  def edit
  end

  def update
    respond_to do |format|
      if @user.update(profile_params)
        format.html { redirect_to profile_path(@user.user_name), flash: { success: 'Profile was successfully updated.' }  }
      else
        flash[:error] = @user.errors.full_messages
        format.html { render :edit }
      end
    end
  end

  private

    def set_user
      @user = User.find_by(user_name:  params[:user_name])
    end

    def authorized_profile
      unless current_user == @user
        redirect_to profile_path(@user.user_name), flash: { alert: 'You are not authorized to edit this profile.'}
      end
    end

    def profile_params
      params.require(:user).permit(:avatar, :bio)
    end

end

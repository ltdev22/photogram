module ProfilesHelper

  def profile_avatar_select user
    if user.avatar?
      return image_tag user.avatar.url, id: 'image-preview', class: 'img-responsive img-circle profile-image'
    end

    image_tag 'default-avatar.png', id: 'image-preview', class: 'img-responsive img-circle profile-image'
  end

end

module PostsHelper

  def form_image_select(post)
    return image_tag post.image.url, id: 'image-preview', class: 'img-responsive' if post.image?
    image_tag 'placeholder.jpg', id: 'image-preview', class: 'img-responsive'
  end

  def liked_post(post)
    if current_user.voted_for? post
      return 'glyphicon-heart'
    end

    'glyphicon-heart-empty'
  end

  def display_likes_for(post)
    votes = post.votes_for.up.by_type('user') # get the likes by users
    if votes.size <= 8
      return likers_list(votes)
    end

    count_likers(votes)
  end

  private

    def likers_list(votes)
      user_names = []

      unless votes.blank?
        votes.voters.each do |voter|
          user_names.push(link_to voter.user_name, profile_path(voter.user_name), class: 'user-name')
        end

        user_names.to_sentence.html_safe + like_plural(votes)
      end
    end

    def count_likers(votes)
      vote_count = votes.size
      vote_count.to_s + ' likes'
    end

    def like_plural(votes)
      if votes.count > 1
        return ' like this'
      end

      ' likes this'
    end

end

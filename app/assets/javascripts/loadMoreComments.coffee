$(document).ready ->
  $('.more-comments').click ->
    $(this).on 'ajax:success', (event, data, status, xhr) ->
      postID = $(this).data('post-id')
      $("#comments_" + postID).html data
      $("#comments-paginator-" + postID).html '<a id=\'more-comments\' data-post-id=' + postID + ' data-type=\'html\' data-remote=\'true\' href=\'/posts/' + postID + '/comments>More comments</a>'
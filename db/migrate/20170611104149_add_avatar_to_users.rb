class AddAvatarToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :avatar, :string, :after => :email
    add_column :users, :bio, :text, :after => :avatar
  end
end

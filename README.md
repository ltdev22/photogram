# Photogram (demo)

This is a clone of Instagram, developed in Ruby on Rails, based on [Let's Build: Instagram](https://www.devwalks.com) tutorial from Ben Walker.

### Instructions

* Clone the repository on your local machine

* Run the `bundle` command through your terminal

* Run the `rails db:migrate` command for migrations
